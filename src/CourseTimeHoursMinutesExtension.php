<?php

namespace Drupal\trinion_suo;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
class CourseTimeHoursMinutesExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new TwigFunction('plural', [$this, 'getPlural']),
    );
  }

  public function getPlural($n, $word1, $word2, $word3) {
    return $n %10 == 1 && $n % 100 != 11 ? $word1 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $word2 : $word3);
  }
}
