<?php

namespace Drupal\trinion_suo\EventSubscriber;

use Drupal\trinion_cart\Event\PaymentEvent;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * TrinionCourse event subscriber.
 */
class TrinionSuoSubscriber implements EventSubscriberInterface {

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onPaymentSuccess(PaymentEvent $event) {
    $zakaz = $event->getOrder();
    foreach ($zakaz->get('field_tp_stroki') as $stroka) {
      $tovar = $stroka->entity->get('field_tp_tovar')->first()->entity;
      if ($tovar->bundle() == 'kurs_obucheniya') {
        \Drupal::service('trinion_main.mails')->uvedomleniePolzovateliaObOplateKursa($zakaz, $tovar);
        $user = User::load($zakaz->get('uid')->getString());
        $user->field_ts_course_access[] = $tovar->get('field_ts_kategoriya_kursa')->getString();
        $user->field_ts_uchenik = 1;
        $user->field_tb_erp_user = 1;
        $user->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvent::PAYMENT_SUCCESS => ['onPaymentSuccess'],
    ];
  }

}
