<?php

namespace Drupal\trinion_suo\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\user\Entity\User;

/**
 * Определение темы для SUO
 */
class TrinionSuoNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    if ($route_name == 'entity.node.canonical') {
      $bundle = $route_match->getParameter('node')->bundle();
      if (in_array($bundle, ['urok_kursa', 'test', ]))
        return TRUE;
    }
    $request_uri = \Drupal::request()->getRequestUri();
    if (preg_match('/^\/obuchenie(\/|$)/', $request_uri))
      return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $theme_name = \Drupal::config('trinion_base.settings')->get('backend_theme_name');
    if (is_null($theme_name))
      $theme_name = 'trinion_backend';
    return $theme_name;
  }
}
