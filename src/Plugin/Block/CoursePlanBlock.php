<?php

namespace Drupal\trinion_suo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a course plan block.
 *
 * @Block(
 *   id = "trinion_suo_course_plan",
 *   admin_label = @Translation("Course plan"),
 *   category = @Translation("Custom")
 * )
 */
class CoursePlanBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node) {
      switch ($node->bundle()) {
        case 'test':
          $urok_nid = $node->get('field_ts_urok')->getString();
          $urok = Node::load($urok_nid);
          $course_tid = $urok->get('field_ts_kategoriya_kursa')->getString();
          $cat = Term::load($course_tid);
          $course_tid = $cat->get('parent')->getString();

          break;
        case 'urok_kursa':
          $course_tid = $node->get('field_ts_kategoriya_kursa')->getString();
          $cat = Term::load($course_tid);
          $course_tid = $cat->get('parent')->getString();
          break;
      }
      if ($course_tid) {
        $categories = \Drupal::service("trinion_suo.course")->getCategories($course_tid);
        $build['content'] = [
          '#theme' => 'course_plan_menu',
          '#categories' => $categories,
        ];
      }
    }

    return $build;
  }

}
